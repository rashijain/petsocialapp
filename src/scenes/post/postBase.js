import React from "react";
import { View, Text } from "react-native";
import * as user from "../../utilities/storage";
import * as api from "../../api/api";
export default class PostBase extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      user_id: "",
      category: "",
      firstname: "",
      lastname: "",
      image: "",
      date: "",
      postList: [],
      categoryList: [],
      likes: [],
      likesCount: 5,
      data: {
        filter: {},
        sort: { date: -1 },
        options: { skip: 0, limit: 4 }
      },
      isLoading: false,
      count: 0
    };
  }
  async componentWillMount() {
    try {
      let userInfo = await user.get("user");
      this.setState({ firstname: userInfo.firstname });
      this.setState({ lastname: userInfo.lastname });
    } catch (err) {
      console.log("there is some error");
    }

    api.get("/post/count").then(data => {
      this.state.count = data.postCount;
    });
  }

  componentDidMount() {
    // let user = localStorage.getItem("user");
    // let userJson = JSON.parse(user);
    // console.log("in the home did mount function");

    // api.get("/category/list").
    //   then((data) => {
    //     this.state.categoryList = data;
    //   }
    //   )
    //   .
    //   catch(() => { })
    this.setState({ isLoading: true });
    api
      .get(
        `/post/list?data=+${encodeURIComponent(
          JSON.stringify(this.state.data)
        )}`
      )
      .then(data => {
        this.setState({ isLoading: false });
        this.setState({ postList: data });
        this.setState({ user_id: userInfo.user_id });
        // $(window).scroll(this.scrollPaging)
      });
  }
  getPosts = (filter, sort, skip) => {
    this.state.data.filter = filter;
    this.state.data.sort = sort;
    this.state.data.options.skip = skip;
    this.setState({ isLoading: true });
    api
      .get(
        `/post/list?data=+${encodeURIComponent(
          JSON.stringify(this.state.data)
        )}`
      )
      .then(data => {
        // window.scrollTo(0, 0);
        this.setState({ isLoading: false });
        this.setState({ postList: data });
      });
  };
  addLike = postid => {
    // let user = localStorage.getItem("user");
    // let userJson = JSON.parse(user);
    let data = {
      post_id: postid,
      user_id: this.state.user_id,
      email: this.state.username
    };
    api
      .post("/post/addLike", data)
      .then(data => {
        this.state.likesCount++;
        console.log("likes", this.state.likesCount);
        this.setState({});
      })
      .catch(() => {});
  };

  //   paging(value) {
  //     if (value == "next") {
  //       window.scrollTo(0, 0);
  //       this.state.data.options.skip += this.state.data.options.limit;
  //       this.getPosts(this.state.data.filter, this.state.data.sort, this.state.data.options.skip);
  //       //this.setState({});
  //     }
  //     if (value == "previous") {
  //       window.scrollTo(0, 0);

  //       this.state.data.options.skip -= this.state.data.options.limit;
  //       this.getPosts(this.state.data.filter, this.state.data.sort, this.state.data.options.skip);
  //       //this.setState({});
  //     }
  //   }

  scrollPaging = () => {
    console.log("scrollpaging called");

    // const windowScrollTop = $(window).scrollTop();
    // const windowHeight = $(window).height();
    // const documentHeight = $(document).height();
    // const scrollHeight = windowScrollTop + windowHeight;

    // const fetchNewDataHeight = 5;
    // if (scrollHeight + fetchNewDataHeight >= documentHeight) {
    // console.log("in the if condition odf scroll paging function");

    if (
      this.state.data.options.skip + this.state.data.options.limit <
      this.state.count
    ) {
      this.state.data.options.skip += this.state.data.options.limit;
      api
        .get(
          `/post/list?data=+${encodeURIComponent(
            JSON.stringify(this.state.data)
          )}`
        )
        .then(data => {
          this.state.postList = this.state.postList.concat(data);
          this.setState({ isLoading: false });
          this.setState({});
        });
    }
  };
}
