import React from "react";
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  Alert,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import { InputField } from "../../components/inputField";
import { SubmitButton } from "../../components/button";
import Base from "./signupBase";
export default class SignUp extends Base {
  render() {
    const { navigation } = this.props;
    return (
      <KeyboardAvoidingView keyboardShouldPersistTaps={"always"}>
        <ScrollView keyboardShouldPersistTaps={"always"}>
          <View style={styles.parent}>
            <View style={styles.header}>
              {/* <Image source={{uri:''}} /> */}
              <Text style={styles.title}>Register</Text>
            </View>
            <View style={styles.signup}>
              <InputField
                placeholder="User Name"
                value={this.state.username}
                onChangeText={e => {
                  this.handleChange("username", e);
                }}
                style={{ marginTop: 30 }}
              />
              <InputField
                placeholder="First Name"
                value={this.state.firstname}
                onChangeText={e => {
                  this.handleChange("firstname", e);
                }}
              />
              <InputField
                placeholder="Last Name"
                value={this.state.lastname}
                onChangeText={e => {
                  this.handleChange("lastname", e);
                }}
              />
              <InputField
                placeholder="Email"
                value={this.state.email}
                onChangeText={e => {
                  this.handleChange("email", e);
                }}
              />
              {(this.state.error && <Text> {this.state.error} </Text>) || (
                <Text />
              )}
              <InputField
                placeholder="Password"
                value={this.state.password}
                onChangeText={e => {
                  this.handleChange("password", e);
                }}
                secureTextEntry={true}
              />
              {(this.state.noError && <Text> {this.state.noError} </Text>) || (
                <Text />
              )}

              <SubmitButton
                value="Sign Up"
                onPress={e => {
                  this.registerFunc(e);
                }}
              />
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("Login");
                }}
              >
                <Text>I already have an account</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  parent: {
    flex: 1
  },
  header: {
    height: 100,
    backgroundColor: "#3D3B3B"
  },
  signup: {
    flex: 2,
    flexDirection: "column",
    alignItems: "center"
  },
  title: {
    marginTop: 20,
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
    color: "white"
  },
  registerLine: {
    marginTop: 50,
    backgroundColor: "orange"
  }
});
